package com.dmercs.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dmercs.main.pojo.TechStackPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Htmlparser {

	public static void main(String[] args) {
		Htmlparser hp = new Htmlparser();
		hp.printDetails();
	}

public void printDetails() {
	Document doc;
	TechStackPojo[] tsPojo = new TechStackPojo[3];
	
	try {
		doc = Jsoup.connect("https://github.com/egis/handbook/blob/master/Tech-Stack.md").get();
		Element content = doc.getElementById("readme");
		Elements h2 = content.getElementsByTag("h2");
		int h2counter = 0;
		
		for (Element item : h2) {
		
			if (h2counter == 3){
				break;
			}
			
			String header = new String(item.toString().substring(item.toString().indexOf("</a>") + 4, item.toString().indexOf("</h2>")));
			tsPojo[h2counter] = new TechStackPojo();
			tsPojo[h2counter].setName(header);
			h2counter++;
		}
		
		Elements tables = content.getElementsByTag("table");
		int cnt = 0;

		for (Element table : tables) {
			if (cnt == 3) {
				break;
			}
			
			List<String> itemnames = new ArrayList<String>();
			Elements body = table.getElementsByTag("tbody");

			for (Element tbody : body) {
				Elements tr = tbody.getElementsByTag("tr");
				for (Element trList : tr) {
					Elements td = trList.getElementsByTag("td");
					String item = td.get(0).html().toString();
					item = item.replaceAll("<strong>", "");					
					item = item.replaceAll("</strong>", "");
					
					if (item != null && !item.isEmpty()){
						itemnames.add(item);	
					}
				}
			}
			
			tsPojo[cnt].setTechStack(itemnames);
			++cnt;
		}
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String finalOutput = gson.toJson(tsPojo);
		
		System.out.println(finalOutput);

	} catch (Exception e) {
		e.printStackTrace();
	}
}
}