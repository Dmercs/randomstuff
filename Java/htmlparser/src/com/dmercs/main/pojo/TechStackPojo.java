package com.dmercs.main.pojo;

import java.util.List;

public class TechStackPojo {

	String name ;
	List<String> techStack;
	
	public void TechStackPojo (String inputName , List<String> stringList) {
		this.name = inputName;
		this.techStack = stringList;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getTechStack() {
		return techStack;
	}
	public void setTechStack(List<String> techStack) {
		this.techStack = techStack;
	} 
}