package com.dmercs.helpers;

import com.dmercs.pojo.LoginInput;
import com.dmercs.pojo.LoginResponse;

public abstract class Helper {

	
	public static LoginResponse validLogin(LoginInput loginInput) 
	{
		LoginResponse loginResponse = new LoginResponse();
		
		if (loginInput.getUsername().toString().equals("user") && loginInput.getPassword().toString().equals("password")) {
			loginResponse.setToken("Token1");
			loginResponse.setValidLogin("Valid");
			loginResponse.setMessage("Valid Login");			
		}
		else {
			loginResponse.setMessage("Username or password is incorrect");
		}
		
		return loginResponse ;
	}
	
}
