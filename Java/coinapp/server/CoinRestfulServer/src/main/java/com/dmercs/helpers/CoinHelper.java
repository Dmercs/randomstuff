package com.dmercs.helpers;

import com.dmercs.pojo.CoinInput;
import com.dmercs.pojo.CoinResponse;

public  abstract class CoinHelper {

	private static float[] currency = new float[] {100,50,20,10,5,2,1,0.50f,0.20f,0.10f,0.05f};
	
	public static CoinResponse calculateChange(CoinInput coinInput) {
		CoinResponse coinResponse = new CoinResponse();
		
		StringBuilder sb = new StringBuilder();
		int valueCount = 0; 
		float change = coinInput.getTotalAmount() - coinInput.getAmountDue();
		
		if (change > 0.0f) {
			
			for (int i = 0 ; i < currency.length ; i++) {
				if (change >= currency[i]) {
					valueCount = (int) (change / currency[i]) ;					
					change = change - (valueCount * currency[i]);					
					sb.append(valueCount );
					if (currency[i] > 0.99f) {
						sb.append(" x R" + currency[i]);
						sb.append("\n");						
					}
					else {
						sb.append(" x " + ( currency[i] * 100 ) + " cent" + "\n");
					}
					
					coinResponse.setHasOutput(true);
				}				
			}
			
			sb.append("\n").append("Total R" + (coinInput.getTotalAmount() - coinInput.getAmountDue()));
		}
		
		coinResponse.setChangeBreakDown(sb.toString());
		return coinResponse;
	}
}