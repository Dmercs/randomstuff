package com.dmercs.pojo;

import java.io.Serializable;

public class LoginResponse implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String token;
	private String validLogin;
	private String message;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getValidLogin() {
		return validLogin;
	}
	public void setValidLogin(String validLogin) {
		this.validLogin = validLogin;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
