package com.dmercs.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.dmercs.helpers.CoinHelper;
import com.dmercs.helpers.Helper;
import com.dmercs.pojo.CoinInput;
import com.dmercs.pojo.CoinResponse;
import com.dmercs.pojo.LoginInput;
import com.dmercs.pojo.LoginResponse;

@Path("/coin")
public class LoginService {

	@GET
	@Path("/login/get")
	@Produces("application/json")
	public LoginResponse loginGet() {
		
		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setMessage("POST request required to login");
		loginResponse.setToken("NoToken");
		loginResponse.setValidLogin("InValid Login");
		return loginResponse;
	}

	@POST
	@Path("/login/post")
	@Consumes("application/json")
	@Produces("application/json")
	public LoginResponse loginPost(LoginInput loginInput) {

		LoginResponse loginResponse = new LoginResponse();
		loginResponse = Helper.validLogin(loginInput);
		return loginResponse;		
	}	
	
	@POST
	@Path("/change/post")
	@Produces("application/json")
	@Consumes("application/json")
	public CoinResponse loginGet(CoinInput coinInput) {
		
		CoinResponse coinResponse = new CoinResponse();
		
		if (coinInput != null) {
			coinResponse = CoinHelper.calculateChange(coinInput);
		}
		else {
			coinResponse.setHasOutput(false);
		}
		
		return coinResponse;		
	}
}	
