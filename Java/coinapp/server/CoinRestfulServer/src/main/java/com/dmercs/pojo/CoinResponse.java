package com.dmercs.pojo;

import java.io.Serializable;

public class CoinResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String changeBreakDown;
	private boolean hasOutput;
	public String getChangeBreakDown() {
		return changeBreakDown;
	}
	public void setChangeBreakDown(String changeBreakDown) {
		this.changeBreakDown = changeBreakDown;
	}
	public boolean isHasOutput() {
		return hasOutput;
	}
	public void setHasOutput(boolean hasOutput) {
		this.hasOutput = hasOutput;
	}

}