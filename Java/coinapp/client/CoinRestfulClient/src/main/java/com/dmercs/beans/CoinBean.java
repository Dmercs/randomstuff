package com.dmercs.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.dmercs.helper.RestfulClientHelper;
import com.dmercs.pojo.CoinInput;
import com.dmercs.pojo.CoinResponse;

@ManagedBean(name = "coinBean", eager = true)
@SessionScoped

public class CoinBean {

	private float totalAmount ;
	private	float dueAmount;
	private String change;
	
	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public float getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(float dueAmount) {
		this.dueAmount = dueAmount;
	}

	public CoinBean() {
		
	
}
	
	

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String coinSubmitAction() {
		CoinInput coinInput = new CoinInput() ;
		CoinResponse coinResponse = new CoinResponse();

		if (dueAmount == 0.0f && totalAmount == 0.0f) {
			FacesMessage message = new FacesMessage("Input a value");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("loginForm:totalAmount", message);
			
		}
		else {
			coinInput.setAmountDue(dueAmount);
			coinInput.setTotalAmount(totalAmount);
			coinInput.setToken("notusedcurrently");
			
			coinResponse = RestfulClientHelper.coinHelper(coinInput);
			
			setChange(coinResponse.getChangeBreakDown());
		}
		
		return "";
	}
	
}
