package com.dmercs.pojo;

import java.io.Serializable;

public class CoinInput implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private float totalAmount ;
	private float amountDue;
	private String token;
	
	public float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
	public float getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(float amountDue) {
		this.amountDue = amountDue;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
