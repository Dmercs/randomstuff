package com.dmercs.helper;

import com.dmercs.pojo.CoinInput;
import com.dmercs.pojo.CoinResponse;
import com.dmercs.pojo.LoginInput;
import com.dmercs.pojo.LoginResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;


public abstract class RestfulClientHelper {

	public static LoginResponse loginHelper (LoginInput input) {
		LoginResponse loginResponse = new LoginResponse();
		
		try {
			ClientRequest request = new ClientRequest("http://localhost:8080/CoinRestfulServer/coin/login/post") ;
			request.accept("application/json");
			
			request.body("application/json", input);
			
			ClientResponse<LoginResponse> response = request.post(LoginResponse.class);
			
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
				
			}
			
			loginResponse.setMessage(response.getEntity().getMessage());
			loginResponse.setToken(response.getEntity().getToken());
			loginResponse.setValidLogin(response.getEntity().getValidLogin());
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
		return loginResponse;
	}
	
	public static CoinResponse coinHelper(CoinInput coinInput ) {
		CoinResponse coinResponse = new CoinResponse();
		
		try {
			
			ClientRequest request = new ClientRequest("http://localhost:8080/CoinRestfulServer/coin/change/post") ;
			request.accept("application/json");
			
			request.body("application/json", coinInput);
			
			ClientResponse<CoinResponse> response = request.post(CoinResponse.class);
			
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
				
			}
			
			coinResponse.setChangeBreakDown(response.getEntity().getChangeBreakDown());
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return coinResponse;
	}
}
