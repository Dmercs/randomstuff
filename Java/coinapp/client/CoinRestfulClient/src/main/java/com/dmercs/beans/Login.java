package com.dmercs.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.dmercs.helper.RestfulClientHelper;
import com.dmercs.pojo.LoginInput;
import com.dmercs.pojo.LoginResponse;

@ManagedBean(name = "loginBean", eager = true)
@SessionScoped

public class Login {

	private String username;
	private String password;
	private LoginInput loginInput;
	private LoginResponse loginResponse;
	
	
	public Login() {
		
	}
	
	public String loginAction() {
		loginInput = new LoginInput();
		loginResponse = new LoginResponse();
		
		if (username != null && password != null) {
			loginInput.setPassword(password);
			loginInput.setUsername(username);
			
		}
		else {
			FacesMessage message = new FacesMessage("Username or password is incorrect");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("loginForm:password", message);

			return "";
		}
		
		loginResponse = RestfulClientHelper.loginHelper(loginInput);
		
		if (loginResponse.getMessage() != null) {
			if(loginResponse.getMessage().toString().equals("Valid Login")) {
				//Set other managed bean
				
				//Direct to main screen
				return "cashdispense";
			}
			else {
				FacesMessage message = new FacesMessage("Username or password is incorrect");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage("loginForm:password", message);

				return "";
			}
		}
		else {
			//Display error message on screen
			return "";
		}
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginInput getLoginInput() {
		return loginInput;
	}

	public void setLoginInput(LoginInput loginInput) {
		this.loginInput = loginInput;
	}

	public LoginResponse getLoginResponse() {
		return loginResponse;
	}

	public void setLoginResponse(LoginResponse loginResponse) {
		this.loginResponse = loginResponse;
	}
	
	
}
