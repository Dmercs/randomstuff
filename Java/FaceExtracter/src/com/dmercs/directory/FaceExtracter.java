package com.dmercs.directory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;

import javax.imageio.ImageIO;

public class FaceExtracter {
	static int cnt = 1;
	static FileWriter writer ; 
	static PrintWriter printWriter;
	static StringBuilder sb ;
	
	public static void main(String[] args) {
				   
		FaceExtracter fe = new FaceExtracter();		
		System.out.println("Finished");
	}
	
	public static void listFilesForFolder(final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
				try {
					printWriter.print( fileEntry.getCanonicalFile() + "\n" );
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
	    }
	}
	
	public static void readInputFile() {
		//Read the input file , get a list of all the files
		//Get the count value to be displayed
		//Update the count based on the
		String filename = "";
		
		sb.append("filename");
		
	}
	
	public static void extractFaces() {
		//Call the Opencv method to extract the faces
		//Write each faces to a seperate file
		//Remove the current file name for the string builder
		//Write the string builder to file incase program crashes
		//Update the count calculate the time remaining and update screen with the number of faces found
		
		String filename = "";
		BufferedImage img = null;
		
		writeFace( filename , img);
		
		
		writeCheckPoint();
		
	}
	
	public static void writeCheckPoint() {
		//Write out the sb builder for checkout for list
	}
	
	public static void writeFace(String filename , BufferedImage img) {
		//Write out a jpg image of the face with the supplied filename
		
		File outputfile = new File("E:\\FaceOutput\\" + filename + ".jpg");
	    try {
			ImageIO.write(img, "jpg", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	
	public FaceExtracter() {
		String startDirectory = "E:\\GDrive";
		File startFile = new File(startDirectory);
		sb = new StringBuilder();
		
		File outPutFile = new File("E:\\\\GDrive\\\\filelist.txt");
		
		if (!outPutFile.exists()) {
			System.out.println("File Exisits");
			
			readInputFile();
			extractFaces();
			
		}
		else {
			//long startTime = System.nanoTime();  
			long startTime = System.currentTimeMillis();
			try {
				writer = new FileWriter("E:\\GDrive\\filelist.txt");			
				printWriter = new PrintWriter(writer);
				listFilesForFolder(startFile);
			}
			catch (Exception ex) {
				
			}
			finally {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				printWriter.flush();
				printWriter.close();
			}
			
			long duration = System.currentTimeMillis() - startTime;
			System.out.println("Time taken : " + duration /1000d + "s");
		}
	
	}
}